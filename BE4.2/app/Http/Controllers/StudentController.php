<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\mahasiswa;
use JWTAuth;

class StudentController extends Controller
{
    function getStudent(){
        $studentList = mahasiswa::get();

        return response()->json($studentList, 200);
    }

    function addStudent(Request $request){
        DB::beginTransaction();
        try{
            $name = $request->input('name');
            $address = $request->input('alamat');
            $studentID = $request->input('studentID');
            $courseID = $request->input('courseID');

            $student = new StudentList;
            $student->name = $name;
            $student->address = $address;
            $student->studentID = $studentID;
            $student->courseID = $courseID;
            $student->save();

            $studentList = mahasiswa::get();

            DB::commit();
            return response()->json($studentList, 201);
        }

        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["nonono" => $e->getMessage()], 500);
        }
    }
}
