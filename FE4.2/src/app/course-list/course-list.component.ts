import { Component, OnInit } from '@angular/core';
import { ApiService } from "../api.service";

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  constructor(private api:ApiService) { }

  course:string = "";
  assignment:string = "";

  CourseList:object[];

  ngOnInit() {
    this.api.getCourseList()
    .subscribe(result => this.CourseList = result);
  }

  addCourseList() {
    this.api.addCourseList(this.course, this.assignment)
    .subscribe(result => this.CourseList = result);

    this.course = "",
    this.assignment = ""
  }

}
