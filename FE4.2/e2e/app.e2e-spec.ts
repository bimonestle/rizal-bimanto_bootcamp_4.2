import { FE4.2Page } from './app.po';

describe('fe4.2 App', () => {
  let page: FE4.2Page;

  beforeEach(() => {
    page = new FE4.2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
